"""insert data to User and rel_user

Revision ID: 31c124d289b0
Revises: 00000000
Create Date: 2019-04-02 08:12:57.647216

"""

# revision identifiers, used by Alembic.
revision = '31c124d289b0'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa

# +----------------+-----------------+
# +----------------+-----------------+

def upgrade():
    
    # * Increase the point_balance for user 1 to 5000
    # * Add a location for user 2, assuming they live in the USA
    # * Change the tier for user 3 to Silver
  
    _sql = """INSERT INTO rel_user (user_id, rel_lookup, attribute, create_time)
            VALUES ({0},'{1}', '{2}', current_timestamp)
    """.format(2, 'Location', 'USA')

    op.execute(_sql)
 
    _sql = """ UPDATE user set point_balance=5000 where user_id=1"""
    op.execute(_sql)

    _sql = """ UPDATE user set tier='Silver' where user_id=3"""
    op.execute(_sql)



def downgrade():

    _sql = """ UPDATE user set point_balance=0 where user_id=1"""
    op.execute(_sql)

    _sql = """ UPDATE user set tier='Carbon' where user_id=1"""
    op.execute(_sql)

    _sql = "DELETE FROM rel_user where user_id=2"
    op.execute(_sql)
