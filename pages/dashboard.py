from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User
from models._helpers import db 



@app.route('/dashboard', methods=['GET'])
def dashboard():

    login_user(User.query.get(1))

    args = {
            'gift_card_eligible': True,
            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
    }
    return render_template("dashboard.html", **args)


@app.route('/community', methods=['GET'])
def community():
    
    login_user(User.query.get(1))

    _sql = '''SELECT u.username, u.point_balance, u.tier, r.attribute, d.attribute
              FROM user u
              LEFT OUTER JOIN rel_user_multi r
              ON r.user_id=u.user_id
              LEFT OUTER JOIN rel_user d  
              ON d.user_id = u.user_id

              order by u.user_id
              limit 5 
              
    '''

    qry = db.session.execute(_sql)
    return render_template('community.html', data=qry)

